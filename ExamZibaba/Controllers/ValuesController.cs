﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ExamZibaba.Entites;

namespace ExamZibaba.Controllers
{
    public class ValuesController : ApiController
    {

        private List<MyEntity> _myList = new List<MyEntity>
        {
            new MyEntity {Text = "An option"},
            new MyEntity {Text = "Another option"},
            new MyEntity {Text = "Here's a looongglooongglooongglooongg option"},
            new MyEntity {Text = "alskdmlaksmdlksa"},
            new MyEntity {Text = "An askjdalsdjlksajdlaksjdlkasjdlaksjdladksjdlksajdlaksjdlaksjdlka"},
        };

        [HttpGet]
        public List<MyEntity> GetMyEntitiesList()
        {
            return _myList;
        }
    }
}