﻿myApp.controller('indexController', function($scope, getData) {
    $scope.options = getData.query();

    $scope.setText = function(txt) {
        $scope.txt = txt;
    }

    $scope.toggleDropDown = function() {
        $scope.showDropDown = !$scope.showDropDown;
    }
});